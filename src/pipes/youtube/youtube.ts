import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'youtube',
})
export class YoutubePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer ){}
  /**
   * Takes a url and sanitizes it
   */
  transform(value: string, ...args) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(value);
  }
}
