import { Injectable } from '@angular/core';

@Injectable()
export class TablesData{

  public tables : any []  = [
    {
      name: 'Açúcares',
      calPerPortion: 55,
      items: [ 
        { food: 'Gelatina Light',
          portion: '2 Porções'
        },
        { food: 'Água de Coco',
          portion: '250ml'
        }
      ]
    },
    {
      name: 'Carboidratos',
      calPerPortion: 25,
      items: [ 
        { food: 'Pão branco',
          portion: '4 fatias'
        },
        { food: 'Pão integral',
          portion: '2 fatias'
        }
      ]
    },
    {
      name: 'Carnes',
      calPerPortion: 65,
      items: [ 
        { food: 'Almôndega',
          portion: '1 Unidade Média'
        },
        { food: 'Atum em Lata',
          portion: '1 Colher de Sopa'
        },
        { food: 'Atum em Lata Conserva em Água',
          portion: '1 Colher de Sopa'
        }
      ]
    },
    {
      name: 'Frutas',
      calPerPortion: 30,
      items: [ 
        { food: 'Pêra',
          portion: '1/2 Unidade'
        },
        { food: 'Pêssego',
          portion: '1 Unidade Pequena'
        },
        { food: 'Salada de Frutas (Banana, Maçã, Laranja, Mamão)',
          portion: '1/4 de Xícara de Chá'
        },
        { food: 'Suco de Abacaxi',
          portion: '1/2 Copo'
        },
        { food: 'Suco de Laranja',
          portion: '1/2 Copo'
        },
        { food: 'Suco de Melão',
          portion: '1/2 Copo'
        },
        { food: 'Suco de Tangerina',
          portion: '1/2 Copo Requeijão'
        },
        { food: 'Tamarindo',
          portion: '6 Unidades'
        },
        { food: 'Tangerina / Mexirica',
          portion: '6 Gomos'
        },
        { food: 'Uva Comum',
          portion: '11 Grãos'
        },
        { food: 'Uva Itália',
          portion: '4 Grãos'
        },
        { food: 'Uva-Rubi',
          portion: '4 Uvas'
        }
      ]
    },
    {
      name: 'Gorduras',
      calPerPortion: 37,
      items: [ 
        { food: 'Amêndoas',
          portion: '6 Unidades'
        },
        { food: 'Azeite de Dendê',
          portion: '1/3 Colher de Sopa'
        },
        { food: 'Azeite de Oliva',
          portion: '2 Colheres de Chá'
        },
        { food: 'Bacon (Gordura)',
          portion: '1/4 Fatia'
        },
        { food: 'Banha de Porco',
          portion: '1/4 Colher de Sopa'
        },
        { food: 'Castanha de Caju',
          portion: '2 Unidades'
        },
        { food: 'Creme Vegetal',
          portion: '1 Colher de Chá'
        },
        { food: 'Halvarina',
          portion: '1/2 Colher de Sopa'
        },
        { food: 'Leite de Amêndoas',
          portion: '1 Xícara'
        },
        { food: 'Leite de Coco',
          portion: '1 Colher de Sopa'
        },
        { food: 'Linhaça (Farinha)',
          portion: '1 Colher de Sopa'
        },
        { food: 'Maionese',
          portion: '1 Colher de Chá'
        },
        { food: 'Maionese Light',
          portion: '2 Colheres de Chá'
        },
        { food: 'Manteiga',
          portion: '1 Colher de Chá'
        },
        { food: 'Margarina Light',
          portion: '2 Colheres de Chá'
        },
        { food: 'Margarina Líquida',
          portion: '1/2 Colher de Sopa'
        },
        { food: 'Margarina Vegetal',
          portion: '1 Colher de Chá'
        },
        { food: 'Molho Shoyo Light',
          portion: '7 Colheres de Sopa'
        },
        { food: 'Nozes',
          portion: '1 Unidade'
        },
        { food: 'Requeijão',
          portion: '2 Colheres de Chá'
        },
        { food: 'Requeijão Light',
          portion: '3 Colheres de Chá'
        },
        { food: 'Óleo Vegetal',
          portion: '2 Colheres de Chá'
        },
        { food: 'Óleo Vegetal Composto de Soja e Oliva',
          portion: '1/2 Colher de Sopa'
        },
        { food: 'Óleo Vegetal de Canola',
          portion: '1/2 Colher de Sopa'
        },
        { food: 'Óleo Vegetal de Girassol',
          portion: '1/2 Colher de Sopa'
        },
        { food: 'Óleo Vegetal de Milho',
          portion: '1/2 Colher de Sopa'
        },
        { food: 'Óleo Vegetal de Soja',
          portion: '1/2 Colher de Sopa'
        }
      ]
    },
    {
      name: 'Laticínios', //tabela incompleta
      calPerPortion: 120,
      items: [ 
        { food: 'Toddynho (Achocolatado)',
          portion: '1/2 Frasco'
        },
        { food: 'Bebida Láctea',
          portion: '1 Frasco'
        }
      ]
    },
    {
      name: 'Verduras',
      calPerPortion: 8,
      items: [ 
        { food: 'Abobrinha Cozida',
          portion: '1 1/2 Colher de Sopa'
        },
        { food: 'Abóbora Cozida',
          portion: '1 Colher de Sobremesa'
        },
        { food: 'Acelga Cozida',
          portion: '1 1/2 Colher de Sopa'
        },
        { food: 'Acelga Crua (Picada)',
          portion: '5 Colheres de Sopa'
        },
        { food: 'Agrião',
          portion: '11 Ramos'
        },
        { food: 'Aipo Cru',
          portion: '1 Unidade'
        },
      ]
    }
  ]
}