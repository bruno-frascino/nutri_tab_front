
export interface MessageInterface {

    id?: number,
    message?: string,
    createDate?: string,
    updateDate?: string
}