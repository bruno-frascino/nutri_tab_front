import { MessageInterface } from './message.interface';

export class Message implements MessageInterface{
  
  constructor( 
    public id?,
    public message?,
    public createDate?,
    public updateDate?
  ){}
}