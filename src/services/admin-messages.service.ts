import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MessageInterface } from '../model/message.interface';

@Injectable()
export class AdminMessagesService {

  private baseURL:string  = 'http://localhost:8180/nutrimais';
  // private baseURL:string  = 'http://138.197.81.222:8080/nutrimais';
  
  constructor(private http: HttpClient){
  }

  public listAll(): Observable<any>{
    return this.http.get<MessageInterface[]>(`${this.baseURL}/rest/messages`);
  }

  public add(message: MessageInterface): Observable<any>{
    return this.http.post(`${this.baseURL}/rest/messages`,
    message,
    { headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  public update(message: MessageInterface): Observable<any>{
    return this.http.put(`${this.baseURL}/rest/messages`,
    message,
    { headers: new HttpHeaders().set('Content-Type', 'application/json')
    });
  }

  public delete(message: MessageInterface): Observable<any>{
    return this.http.delete(`${this.baseURL}/rest/messages/${message.id}`
    );
  }

}