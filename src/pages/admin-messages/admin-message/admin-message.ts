import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Message } from '../../../model/message.model';
import { MessageInterface} from '../../../model/message.interface';
import { AdminMessagesService } from '../../../services/admin-messages.service';

@IonicPage()
@Component({
  selector: 'page-admin-message',
  templateUrl: 'admin-message.html',
})
export class AdminMessagePage implements OnInit {

  private adminMessageForm: FormGroup;
  private message: MessageInterface;
  private messages: MessageInterface[];

  private msgIndex: number = -1;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private fb: FormBuilder,
    private messagesService: AdminMessagesService ) {

    this.messages = navParams.data[0];//messages
    let msg = navParams.data[1];//message
    this.message = new Message();

    if( msg ){
      this.message = msg;
      this.msgIndex = this.messages.indexOf(msg);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminMessagePage');
  }

  ngOnInit(){
    this.buildForm();
  }

  private save(){
    this.message = this.adminMessageForm.value;

    //edit mode
    if( this.msgIndex > -1 ){
      
      this.messagesService.update(this.message)
        .subscribe( data => {
          this.messages[this.msgIndex] = data;
        });
      
    }
    //add
    else{
      this.messagesService.add(this.message)
        .subscribe( data => {
          this.messages.push(data);
        });
    }
    
    this.navCtrl.pop();
  }

  private buildForm(){
    this.adminMessageForm = this.fb.group({
      'id': [this.message.id],
      'message': [this.message.message,[
        Validators.required,
        Validators.maxLength(250)
      ]],
      'createDate': [this.message.createDate]
    });
  }

  private validateField(controlName: string): void {

    this.formErrors[controlName] = [];

    const control = this.adminMessageForm.get(controlName);//field = formControlName

    if (control && control.errors || (control.dirty && !control.valid)) {
      const messages = this.validationMessages[controlName];
      for (const key in control.errors) {
        //this.formErrors[controlName] += messages[key] + ' ';
        this.formErrors[controlName].push(messages[key]);
      }
    }
  }

  private formErrors = {
    'message': []
  };

  private validationMessages = {
    'message': {
      'required': 'Campo obrigatório.',
      'maxlength': 'Campo mensagem não pode ter mais do que 250 caracteres.'
    }
  };
}
