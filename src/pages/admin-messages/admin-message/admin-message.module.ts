import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminMessagePage } from './admin-message';

@NgModule({
  declarations: [
    AdminMessagePage,
  ],
  imports: [
    IonicPageModule.forChild(AdminMessagePage),
  ],
})
export class AdminMessagePageModule {}
