import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MessageInterface } from '../../model/message.interface';
import { AdminMessagesService } from '../../services/admin-messages.service';

@IonicPage()
@Component({
  selector: 'page-admin-messages',
  templateUrl: 'admin-messages.html',
})
export class AdminMessagesPage implements OnInit {

  private messages: MessageInterface[];
  private loadedMessages: MessageInterface[];

  constructor(public navCtrl: NavController, 
      public navParams: NavParams,
      private alertCtrl: AlertController,
      private messagesService: AdminMessagesService ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminMessagesPage');
  }

  public ngOnInit(){

    this.initializeMessages();
  }

  private searchMessages(event){
   
    this.initializeMessages();

    let sData:string = event.target.value;

    if(sData && sData.trim() != '') {
      this.messages = this.messages.filter( item => {
        return item.message.toLowerCase().indexOf(sData.toLowerCase()) > -1;
      });
    }
  }

  private addItem(){
    if(this.messages == null) this.messages = [];
    this.navCtrl.push('AdminMessagePage', [this.messages, null]);
  }

  private editItem(msg){
    this.navCtrl.push('AdminMessagePage', [this.messages, msg]);
  }

  private removeItem(msg){
    let confirm = this.alertCtrl.create({
      title: 'Remover',
      message: `Você gostaria realmente de remover: <strong>${msg.message}</strong>`,
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.messagesService.delete(msg)
              .subscribe( data => {
                const ind = this.messages.indexOf(msg);
                this.messages.splice(ind,1);
              });
          }
        },
        {
          text: 'Não'
        }
      ]
    });
    confirm.present();
  }

  private initializeMessages(){

    if(this.messages == null) {
      this.messagesService.listAll()
        .subscribe( data => {

          if( data && data.length > 0 ){
            this.messages = data;
            this.loadedMessages = data;
          }
        });
    }
    else {
      this.messages = this.loadedMessages;
    }

  }
}
