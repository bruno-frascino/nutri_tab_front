import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TableInterface } from './table/model/table.interface';
import { TablesData } from '../../data/tables.data';

@IonicPage()
@Component({
  selector: 'page-tables',
  templateUrl: 'tables.html',
})
export class TablesPage {

  private tables: TableInterface[];
  private dataTables: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private tabData: TablesData) {
    
      this.initializeTables();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TablesPage');
  }

  // private itemSelected(item: TableInterface){
  private itemSelected(item){
    this.navCtrl.push('TablePage', [item]);
  }
  
  private initializeTables(){

    this.dataTables = this.tabData.tables;

  }

}
