import { TableInterface } from "./table.interface";
import { TableItemInterface } from "./table-item.interface";

export class Table implements TableInterface{
  constructor( 
    public id?: number,
    public group?: string,
    public calPerPortion?: number,
    public table?: TableItemInterface[]
  ){}
}