export interface TableItemInterface{
  id?: number,
  food?: string,
  portion?: string
}