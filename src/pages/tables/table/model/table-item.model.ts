
export class TableItem {
  constructor( 
    public id?: number,
    public food?: string,
    public portion?: string
  ){}
}