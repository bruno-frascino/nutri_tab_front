import { TableItemInterface } from "./table-item.interface";

export interface TableInterface {
  id?: number,
  group?: string,
  calPerPortion?: number,
  items?: TableItemInterface[]
}