import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-table',
  templateUrl: 'table.html',
})
export class TablePage {

  private showPortions: boolean[];
  private group;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.group = this.navParams.data[0];
    this.initializeFlags();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TablePage');
  }

  public togglePortion(ind){
    this.showPortions[ind] = !this.showPortions[ind];
  }

  private initializeFlags(){
    this.showPortions = [];
    this.group.items.forEach(item => {
      this.showPortions.push(false);
    });
  }
}
