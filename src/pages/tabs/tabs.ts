import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root: any = 'TablesPage';
  tab2Root: any = 'InstructionsPage';
  tab3Root: any = 'MessagesPage';
  tab4Root: any = 'AboutUsPage';
  
  myIndex: number;

  constructor(public navParams: NavParams) {
    // Set the active tab based on the passed index from menu.ts
    this.myIndex = navParams.data.tabIndex || 0;
  }

  

}
