import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdminMessagesService } from '../../services/admin-messages.service';
import { MessageInterface } from '../../model/message.interface';

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage implements OnInit{

  private messages: MessageInterface[] = [];

  constructor(public navCtrl: NavController, 
      public navParams: NavParams, 
      private messageService: AdminMessagesService) {
  }

  ngOnInit(){
    
    this.loadMessages();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

  private loadMessages(){
    this.messageService.listAll()
      .subscribe( data => {
        this.messages = data;
      });
  }

}
