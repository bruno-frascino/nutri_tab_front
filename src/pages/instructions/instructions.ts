import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { VideoPlayer, VideoOptions } from '@ionic-native/video-player';
// import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

@IonicPage()
@Component({
  selector: 'page-instructions',
  templateUrl: 'instructions.html',
})
export class InstructionsPage {

  // private videoOptions: VideoOptions;
  // private videoURL: string = 'http://138.197.81.222:8080/videos/test1.mp4';

  constructor(public navCtrl: NavController,
    // private videoPlayer: VideoPlayer,
    // private streamingMedia: StreamingMedia
    ) {
  }

  //only when first loaded to cache
  ionViewDidLoad() {
    console.log('ionViewDidLoad InstructionsPage');
    const rockBellVideo = <HTMLMediaElement>document.getElementById('rockBell');
    rockBellVideo.autoplay = false;
  }

  
  // onYouTubeIframeAPIReady() {
    // player = new YT.Player('player', {
    //   height: '390',
    //   width: '640',
    //   videoId: 'M7lc1UVf-VE',
    //   events: {
    //     'onReady': onPlayerReady,
    //     'onStateChange': onPlayerStateChange
    //   }
    // });
  // }

  //everytime it becomes active
  ionViewWillEnter(){
    this.videos.forEach(v => {
      const utuB = <HTMLMediaElement>document.getElementById(v.id);
      utuB.src = v.video;
    });    
  }

  ionViewWillLeave(){
    const rockBellVideo = <HTMLMediaElement>document.getElementById('rockBell');
    rockBellVideo.pause();

    //Workaround aka here Hack! 8o
    this.videos.forEach(v => {
      const utuB = <HTMLMediaElement>document.getElementById(v.id);
      utuB.src = "";
    });

    //TO DO
    // let listaFrames = document.getElementsByTagName("iframe");
    // for (var index = 0; index < listaFrames.length; index++) {
    //   let iframe = listaFrames[index].contentWindow;
    //   iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    // }

  }

 private videos: any[] = [
   {
     id: 1,
     title: 'Dicas de Nutrição',
     video: 'https://www.youtube.com/embed/mDPhgRdyo9o?rel=0'
   }
//   {
//     id: 2,
//     title: 'Neno Halloween',
//     video: 'https://www.youtube.com/embed/93tlXl4qtOY?rel=0'
//   }
 ]

  //
  // async playVideo(){

  //   this.videoOptions = {
  //     volume: 0.5
  //   }

  //   // Playing a video.
  //   this.videoPlayer
  //     .play(this.videoURL, this.videoOptions)
  //       .then(() => {
  //         console.log('video completed');
  //       }).catch(err => {
  //         console.log(err);
  //       });
  // }

  // playVideoStreaming(){
  //   let options: StreamingVideoOptions = {
  //     successCallback: () => {
  //       console.log('Successfully Played');
  //     },
  //     errorCallback: () => {
  //       console.log('Something wrong...');
  //     },
  //     orientation: 'portrait'
  //   };

  //   this.streamingMedia.playVideo(this.videoURL, options);

  // }

}
