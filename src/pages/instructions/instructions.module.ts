import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InstructionsPage } from './instructions';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    InstructionsPage,
  ],
  imports: [
    IonicPageModule.forChild(InstructionsPage),
    PipesModule
  ],
})
export class InstructionsPageModule {}
