### Welcome to Nutrimais Front End! 

Nutrimais Front End is a cross platform mobile app with the purpose of helping patients to follow the recommended daily intake of calories recommend by the doctor.

- Version 0.1

### Architecture

This is a Single Page Application(SPA) implemented using Ionic Framework (v3.9.2) with Angular (v4.4.3), intended to be a PWA.

### Dependencies

* Node.js v6.11.3 or higher
* Ionic 3.9.2 - *It's not compatible to Ionic 4
* Nutrimais Back End

### Getting Started

Install Node.js

Install Ionic: 
```bash
$ sudo npm install -g ionic
```
Clone this repository

Inside 'nutri-tab' folder run: 
```bash
$ ionic serve
```

### Contact ###

* Bruno Frascino - bfrascino80@gmail.com